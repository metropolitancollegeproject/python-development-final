#include <iostream>
#include <vector>
#include <string>
#include <ctime>
#include <stdbool.h>
#include <cstdlib>
#include <string.h>
#include <cmath>
using namespace std;

class VernamAlpha
{
	private:
		char realletter1;
		char realletter2;
		string code;
	public:
		VernamAlpha(char xrealletter1,char xrealletter2,string& xcode);
        char getrealletter1();
		char getrealletter2();
		string getcode();
		void setrealletter1(char xrealletter1);
		void setrealletter2(char xrealletter2);
		void setcode(string& xcode);
};

VernamAlpha::VernamAlpha(char xrealletter1,char xrealletter2,string& xcode)
{
	realletter1 = xrealletter1;
	realletter2 = xrealletter2;
	code = xcode;
}

char VernamAlpha::getrealletter1()
{
	return realletter1;
}

char VernamAlpha::getrealletter2()
{
	return realletter2;
}

string VernamAlpha::getcode()
{
	return code;
}

void VernamAlpha::setrealletter1(char xrealletter1)
{
	realletter1 = xrealletter1;
}

void VernamAlpha::setrealletter2(char xrealletter2)
{
	realletter2 = xrealletter2;
}

void VernamAlpha::setcode(string& xcode)
{
	code = xcode;
}

void menu();
void fill_vernam(vector <VernamAlpha>& vernam, char a, char b, string code);
void full_vernam(vector <VernamAlpha>& vernam);
void print_vernam(vector <VernamAlpha>& vernam);
void crypt_vernam(vector <VernamAlpha>& vernam, string& key1, string& key2, string& yourtext);
string xor_vernam(string& key1, string& codex);
void decrypt_vernam(vector <VernamAlpha>& vernam, string& key1, string& key2, string& yourtext);
int sailormoon_transformation(string& string_cut);
void crypt_polyab(string& mytext, int blocknum, vector <int>& keys);
void decrypt_polyab(string& mytext, int blocknum, vector <int>& keys);
void crypt_transcipher(string& mytext, int blocknum, string& key2);
string transposition(string& letters_blocknum,string& key2);
void decrypt_transcipher(string& mytext, int blocknum, string& key2);
string tranxposition(string& letters_blocknum,string& key2);
string fullmoon_transformation(char text);

int main()
{
system("color f1");
	string CPPbug,choice,yourtext,mytext,key1,key2;
	int blocknum;
	
	vector <VernamAlpha> vernam;
	full_vernam(vernam);
	
	vector <int> keys;
	
	while(true)
	{
		menu();
		cin>>choice;
		cout<<endl;
		
		if (choice == "1")
		{
            _Bool flag = 1;
            getline(cin,CPPbug);
			while(flag)
			{
				mytext = "";
				flag=0;
				cout<<" Give a text to encrypt (example SailorMoon) : ";
            	getline(cin,yourtext);
            	for (int i=0;i<yourtext.size();++i)
            	{
					if(((int)yourtext[i]>=65)&&((int)yourtext[i]<=90))
					{
						mytext+=yourtext[i];
					}
					else if (((int)yourtext[i]>=97)&&((int)yourtext[i]<=122))
					{
						mytext+=toupper(yourtext[i]);
					}
					else
					{
						cout<<" ERROR: Only letters are allowed."<<endl<<endl;
						flag=1;
					}
            	}
			}
			//cout<<mytext<<endl;
            
            while(true)
            {
                cout<<" Give the block number (example 4) : ";
				cin>>blocknum;
				if (mytext.size()<blocknum)
				{
					cout<<" ERROR: Block number does not match with the text's size."<<endl<<endl;
				}
				else
				{
					break;
				}
            }
			
			flag = 1;
			getline(cin,CPPbug);
			while(flag)
			{
                keys.clear();
				flag = 0;
				string string_cut = "";
				cout<<" Give the key with "<<blocknum<<" numbers divided by spaces (example 1 11 22 2) : ";
				getline(cin,key1);
				key1 += " ";
				
				for (int i=0;i<key1.size();++i)
				{
					if((int)key1[i] != 32)
					{
						if (((int)key1[i]>=48)&&((int)key1[i]<=57))
						{
                            string_cut += key1[i];
						}
                        else
                        {
                            flag = 1;
							cout<<" ERROR: Only numbers divided by spaces are allowed."<<endl<<endl;
							break;
                        }
					}
					else
					{
						if (string_cut != "")
						{
                            keys.push_back(sailormoon_transformation(string_cut));
							string_cut = "";
						}
					}
				}
				
				if (keys.size() != blocknum)
				{
					flag = 1;
					cout<<" ERROR: Block number does not match with the keys' number."<<endl<<endl;
				}
			}
	
			crypt_polyab(mytext,blocknum,keys);
		}

        if (choice == "2")
		{
            _Bool flag = 1;
            getline(cin,CPPbug);
			while(flag)
			{
				mytext = "";
				flag=0;
				cout<<" Give a text to decrypt (example tlenpciqpy) : ";
            	getline(cin,yourtext);
            	for (int i=0;i<yourtext.size();++i)
            	{
					if(((int)yourtext[i]>=65)&&((int)yourtext[i]<=90))
					{
						mytext+=yourtext[i];
					}
					else if (((int)yourtext[i]>=97)&&((int)yourtext[i]<=122))
					{
						mytext+=toupper(yourtext[i]);
					}
					else
					{
						cout<<" ERROR: Only letters are allowed."<<endl<<endl;
						flag=1;
					}
            	}
			}
			//cout<<mytext<<endl;

            while(true)
            {
                cout<<" Give the block number (example 4) : ";
				cin>>blocknum;
				if (mytext.size()<blocknum)
				{
					cout<<" ERROR: Block number does not match with the text's size."<<endl<<endl;
				}
				else
				{
					break;
				}
            }

			flag = 1;
			getline(cin,CPPbug);
			while(flag)
			{
                keys.clear();
				flag = 0;
				string string_cut = "";
				cout<<" Give the key with "<<blocknum<<" numbers divided by spaces (example 1 11 22 2) : ";
				getline(cin,key1);
				key1 += " ";

				for (int i=0;i<key1.size();++i)
				{
					if((int)key1[i] != 32)
					{
						if (((int)key1[i]>=48)&&((int)key1[i]<=57))
						{
                            string_cut += key1[i];
						}
                        else
                        {
                            flag = 1;
							string_cut = "";
							cout<<" ERROR: Only numbers divided by spaces are allowed."<<endl<<endl;
							break;
                        }
					}
					else
					{
						if (string_cut != "")
						{
                            keys.push_back(sailormoon_transformation(string_cut));
							string_cut = "";
						}
					}
				}

				if (keys.size() != blocknum)
				{
					flag = 1;
					cout<<" ERROR: Block number does not match with the keys' number."<<endl<<endl;
					string_cut = "";
				}
			}

			decrypt_polyab(mytext,blocknum,keys);
		}
		
		if (choice == "3")
		{
            _Bool flag = 1;
            getline(cin,CPPbug);
			while(flag)
			{
				mytext = "";
				flag=0;
				cout<<" Give a text to encrypt (example ThisIsAMessage) : ";
            	getline(cin,yourtext);
            	for (int i=0;i<yourtext.size();++i)
            	{
					if(((int)yourtext[i]>=65)&&((int)yourtext[i]<=90))
					{
						mytext+=yourtext[i];
					}
					else if (((int)yourtext[i]>=97)&&((int)yourtext[i]<=122))
					{
						mytext+=toupper(yourtext[i]);
					}
					else
					{
						cout<<" ERROR: Only letters are allowed."<<endl<<endl;
						flag=1;
					}
            	}
			}
			//cout<<mytext<<endl;

            while(true)
            {
                cout<<" Give the block number (example 5) : ";
				cin>>blocknum;
				if (mytext.size()<blocknum)
				{
					cout<<" ERROR: Block number does not match with the text's size."<<endl<<endl;
				}
				else
				{
					break;
				}
            }
            
            flag = 1;
			while(flag)
			{
				flag = 0;
                cout<<" Give unique "<<blocknum<<" numbers key for transposition (example 32154): ";
				cin>>key2;
				for (int i=0;i<blocknum;++i)
				{
					if (((int)key2[i]-48<1)||((int)key2[i]-48>blocknum)||(key2.size()!=blocknum))
					{
						flag = 1;
					}
					
					for (int j=blocknum;j>i;--j)
					{
						if (key2[i]==key2[j])
						{
							flag = 1;
							break;
						}
					}
				}
				if (flag)
				{
					cout<<" ERROR: Only "<<blocknum<<" unique numbers 1-"<<blocknum<<" are allowed."<<endl<<endl;
				}
			}
			
			decrypt_transcipher(mytext, blocknum, key2);
		}
		
		if (choice == "4")
		{
            _Bool flag = 1;
            getline(cin,CPPbug);
			while(flag)
			{
				mytext = "";
				flag=0;
				cout<<" Give a text to decrypt (example ihtismassegase) : ";
            	getline(cin,yourtext);
            	for (int i=0;i<yourtext.size();++i)
            	{
					if(((int)yourtext[i]>=65)&&((int)yourtext[i]<=90))
					{
						mytext+=yourtext[i];
					}
					else if (((int)yourtext[i]>=97)&&((int)yourtext[i]<=122))
					{
						mytext+=toupper(yourtext[i]);
					}
					else
					{
						cout<<" ERROR: Only letters are allowed."<<endl<<endl;
						flag=1;
					}
            	}
			}
			//cout<<mytext<<endl;

            while(true)
            {
                cout<<" Give the block number (example 5) : ";
				cin>>blocknum;
				if (mytext.size()<blocknum)
				{
					cout<<" ERROR: Block number does not match with the text's size."<<endl<<endl;
				}
				else
				{
					break;
				}
            }

            flag = 1;
			while(flag)
			{
				flag = 0;
                cout<<" Give unique "<<blocknum<<" numbers key for transposition (example 32154): ";
				cin>>key2;
				for (int i=0;i<blocknum;++i)
				{
					if (((int)key2[i]-48<1)||((int)key2[i]-48>blocknum)||(key2.size()!=blocknum))
					{
						flag = 1;
					}

					for (int j=blocknum;j>i;--j)
					{
						if (key2[i]==key2[j])
						{
							flag = 1;
							break;
						}
					}
				}
				if (flag)
				{
					cout<<" ERROR: Only "<<blocknum<<" unique numbers 1-"<<blocknum<<" are allowed."<<endl<<endl;
				}
			}

			crypt_transcipher(mytext, blocknum, key2);
		}
		
		if (choice == "5")
		{
            _Bool flag = 1;
			while(flag)
			{
				flag = 0;
                cout<<" Give 5 numbers key in 0-1 for XOR (example 10001): ";
				cin>>key1;
				for (int i=0;i<5;++i)
				{
					if (((key1[i]!='0')&&(key1[i]!='1'))||(key1.size()!=5))
					{
						flag = 1;
						cout<<" ERROR: Only 5 numbers 0-1 are allowed."<<endl<<endl;
						break;
					}
				}
			}
			
			flag = 1;
			while(flag)
			{
				flag = 0;
                cout<<" Give unique 5 numbers key for transposition (example 12345): ";
				cin>>key2;
				for (int i=0;i<5;++i)
				{
					if (((key2[i]!='1')&&(key2[i]!='2')&&(key2[i]!='3')&&(key2[i]!='4')&&(key2[i]!='5'))||(key2.size()!=5))
					{
						flag = 1;
					}
					
					for (int j=5;j>i;--j)
					{
						if (key2[i]==key2[j])
						{
							flag = 1;
							break;
						}
					}
				}
				if (flag)
				{
					cout<<" ERROR: Only 5 unique numbers 1-5 are allowed."<<endl<<endl;
				}
			}
			
			flag = 1;
			while(flag)
			{
				flag = 0;
                cout<<" Give your text (strictly only letters): ";
				cin>>yourtext;
				for (int i=0;i<yourtext.size();++i)
				{
					for (int j=0;j<vernam.size();++j)
					{
                    	if ((yourtext[i]==vernam[j].getrealletter1())||(yourtext[i]==vernam[j].getrealletter2()))
                    	{
							flag = 0;
							break;
                    	}
                    	else
						{
							flag = 1;
                    	}
					}
					if (flag)
					{
                        cout<<" ERROR: Only english letters are allowed."<<endl<<endl;
						break;
					}
				}
			}
			
			crypt_vernam(vernam,key1,key2,yourtext);
		}
		
		if (choice == "6")
		{
            _Bool flag = 1;
			while(flag)
			{
				flag = 0;
                cout<<" Give 5 numbers key in 0-1 for XOR (example 10001): ";
				cin>>key1;
				for (int i=0;i<5;++i)
				{
					if (((key1[i]!='0')&&(key1[i]!='1'))||(key1.size()!=5))
					{
						flag = 1;
						cout<<" ERROR: Only 5 numbers 0-1 are allowed."<<endl<<endl;
						break;
					}
				}
			}

			flag = 1;
			while(flag)
			{
				flag = 0;
                cout<<" Give unique 5 numbers key for transposition (example 12345): ";
				cin>>key2;
				for (int i=0;i<5;++i)
				{
					if (((key2[i]!='1')&&(key2[i]!='2')&&(key2[i]!='3')&&(key2[i]!='4')&&(key2[i]!='5'))||(key2.size()!=5))
					{
						flag = 1;
					}

					for (int j=5;j>i;--j)
					{
						if (key2[i]==key2[j])
						{
							flag = 1;
							break;
						}
					}
				}
				if (flag)
				{
					cout<<" ERROR: Only 5 unique numbers 1-5 are allowed."<<endl<<endl;
				}
			}

			flag = 1;
			while(flag)
			{
				flag = 0;
                cout<<" Give your text (strictly only 0-1 blocks of 5 letters): ";
				cin>>yourtext;
				
				if (yourtext.size()%5!=0) //periorismos block sto 5
				{
					flag = 1;
				}
				
				for (int i=0;i<yourtext.size();++i) //text 0-1
				{
					if ((yourtext[i]!='0')&&(yourtext[i]!='1'))
					{
						flag = 1;
						break;
					}
				}
				
				if (flag)
				{
					cout<<" ERROR: Only 0-1 blocks of 5 letters are allowed."<<endl<<endl;
				}
			}

			decrypt_vernam(vernam,key1,key2,yourtext);
		}
		
		if (choice == "7")
		{
            print_vernam(vernam);
		}
		
		if (choice == "0")
		{
			break;
		}

		if ((choice != "0")&&(choice != "1")&&(choice != "2")&&(choice != "3")&&(choice != "4")&&(choice != "5")&&(choice != "6")&&(choice != "7"))
		{
			cout<<" WRONG CHOICE!"<<endl<<endl;
		}
		
	system("pause");
	}
system("pause");
}

void menu()
{
	system("cls");
	cout<<endl;
	cout<<" Encrypt Polyalphabetic ------------------------------------------- 1"<<endl;
	cout<<" Decrypt Polyalphabetic ------------------------------------------- 2"<<endl;
	cout<<" Encrypt Transposition -------------------------------------------- 3"<<endl;
	cout<<" Decrypt Transposition -------------------------------------------- 4"<<endl;
	cout<<" Encrypt Vernam --------------------------------------------------- 5"<<endl;
	cout<<" Decrypt Vernam --------------------------------------------------- 6"<<endl;
	cout<<" Show Vernam Alpha ------------------------------------------------ 7"<<endl;
	cout<<" Close ------------------------------------------------------------ 0"<<endl;
	cout<<" Choice: ";
}

void fill_vernam(vector <VernamAlpha>& vernam, char a, char b, string code)
{
	VernamAlpha newvernam(a,b,code);
	newvernam.setrealletter1(a);
	newvernam.setrealletter2(b);
	newvernam.setcode(code);
	
	vernam.push_back(newvernam);
}

string fullmoon_transformation(char text)
{
	string newtext;
	char chara[5];
    int num;
    num=text-64;
    for (int j=4;j>=0;--j)
    {
		chara[j]=(num%2)+48;
		num=num/2;
    }
    for(int j=0;j<5;++j)
    {
		newtext+=chara[j];
    }
    cout<<newtext<<endl;
    return newtext;
}

void full_vernam(vector <VernamAlpha>& vernam)
{
	for(int i=65;i<=90;++i)
	{
		fill_vernam(vernam,(char)i,(char)i+32,fullmoon_transformation((char)i));
	}
}

void print_vernam(vector <VernamAlpha>& vernam)
{
	for (int i=0;i<vernam.size();++i)
	{
		cout<<" "<<vernam[i].getrealletter1()<<" or "<<vernam[i].getrealletter2()<<" --> "<<vernam[i].getcode()<<endl;
	}
	cout<<endl;
}

void crypt_vernam(vector <VernamAlpha>& vernam, string& key1, string& key2, string& yourtext)
{
	string newtext = "";
	for (int i=0;i<yourtext.size();++i)
	{
		for (int j=0;j<vernam.size();++j)
		{
            if ((yourtext[i]==vernam[j].getrealletter1())||(yourtext[i]==vernam[j].getrealletter2()))
			{
				string codex = vernam[j].getcode();
				codex = xor_vernam(key1,codex);
				codex = transposition(key2,codex);
				newtext += codex;
            }
		}
	}
	
	cout<<endl<<" XOR KEY : "<<key1<<endl;
	cout<<" TRANSPOSITION KEY : "<<key2<<endl;
	cout<<" NORMAL TEXT : "<<yourtext<<endl;
	cout<<" ENCRYPTED TEXT : "<<newtext<<endl<<endl;
}
string xor_vernam(string& key1, string& codex)
{
	string newcode="";
	for (int i=0;i<5;++i)
	{
		if (codex[i]==key1[i])
		{
			newcode += "0";
		}
		else
		{
			newcode += "1";
		}
	}
	//cout<<" from key1: "<<newcode<<endl;
	return newcode;
}

void decrypt_vernam(vector <VernamAlpha>& vernam, string& key1, string& key2, string& yourtext)
{
	string newtext = "";
	
	_Bool flag = 0;
	string letter_block = "";
	for (int i=0;i<yourtext.size();++i)
	{
        letter_block += yourtext[i];
		if ((i+1)%5 == 0)
		{
			string codex = tranxposition(key2,letter_block);
			codex = xor_vernam(key1,codex);
			for (int j=0;j<vernam.size();++j)
			{
				if (codex == vernam[j].getcode())
				{
					newtext += vernam[j].getrealletter1();
					break;
				}
			}
			letter_block = "";
		}
	}

	cout<<endl<<" XOR KEY : "<<key1<<endl;
	cout<<" TRANSPOSITION KEY : "<<key2<<endl;
	cout<<" ENCRYPTED TEXT : "<<yourtext<<endl;
	cout<<" DECRYPTED TEXT : "<<newtext<<endl<<endl;
}

int sailormoon_transformation(string& string_cut)
{
	int number = 0;
 	for (int i=0;i<string_cut.size();++i)
	{
        number += (string_cut[string_cut.size()-i-1]-48)*pow(10.0,i);
	}
	return number;
}

void crypt_polyab(string& mytext, int blocknum, vector <int>& keys)
{
	vector <int> allkeys;
	int i=0;
	for (int j=0;j<mytext.size();++j)
	{
		allkeys.push_back(keys[i]);
		//cout<<keys[i]<<" "<<allkeys[i]<<endl;
		++i;
		if(i==blocknum)
		{
			i -= blocknum;
		}
	}
	
	string newtext="";
	for (int i=0;i<mytext.size();++i)
	{
		int sum = (int)mytext[i]+allkeys[i];
		while(true)
		{
            if(sum<65)
			{
				sum+=26;
			}
			else if(sum>90)
			{
				sum-=26;
			}
			else
			{
				break;
			}
		}
		newtext += (char)sum;
	}
	
	cout<<endl<<" NORMAL TEXT : "<<mytext<<endl;
	cout<<" BLOCK NUMBER : "<<blocknum<<endl;
	for (int i=0;i<keys.size();++i)
	{
		cout<<" KEY "<<(i+1)<<" : "<<keys[i]<<endl;
	}
	cout<<" ENCRYPTED TEXT : "<<newtext<<endl<<endl;
}

void decrypt_polyab(string& mytext, int blocknum, vector <int>& keys)
{
    vector <int> allkeys;
	int i=0;
	for (int j=0;j<mytext.size();++j)
	{
		allkeys.push_back(keys[i]);
		++i;
		if(i==blocknum)
		{
			i -= blocknum;
		}
	}

	string newtext="";
	for (int i=0;i<mytext.size();++i)
	{
		int sum = (int)mytext[i]-allkeys[i];
		while(true)
		{
            if(sum<65)
			{
				sum+=26;
			}
			else if(sum>90)
			{
				sum-=26;
			}
			else
			{
				break;
			}
		}
		newtext += (char)sum;
	}

	cout<<endl<<" ENCRYPTED TEXT : "<<mytext<<endl;
	cout<<" BLOCK NUMBER : "<<blocknum<<endl;
	for (int i=0;i<keys.size();++i)
	{
		cout<<" KEY "<<(i+1)<<" : "<<keys[i]<<endl;
	}
	cout<<" DECRYPTED TEXT : "<<newtext<<endl<<endl;
}

void crypt_transcipher(string& mytext, int blocknum, string& key2)
{
	string newtext = "";
	string letters_blocknum = "";
	int place = 0;
    for (int i=0;i<mytext.size();++i)
    {
		letters_blocknum += mytext[i];
		if ((i+1)%blocknum == 0)
		{
			newtext += transposition(key2,letters_blocknum);
			letters_blocknum = "";
			place = i;
		}
    }
    
	//what is left from mytext remove the biggest ones
    string keylast="";
    for (int i=0;i<=mytext.size()-place;++i)
    {
		if ((int)key2[i]-48<mytext.size()-place)
		{
			keylast += key2[i]; //create new key for last letters
		}
    }

	for (int i=place;i<mytext.size();++i)
    {
        letters_blocknum += mytext[i]; //create new stringwith the size of last letters
	}
	newtext += transposition(keylast,letters_blocknum); //transposition letters depending ot he key
	
	cout<<endl<<" GIVEN TEXT : "<<mytext<<endl;
	cout<<" BLOCK NUMBER : "<<blocknum<<endl;
	cout<<" KEY : "<<key2<<endl;
	cout<<" FINAL TEXT : "<<newtext<<endl<<endl;
}

string transposition(string& key2, string& letters_blocknum)
{
    string newcode="";
	for (int j=0;j<key2.size();++j)
	{
		for (int i=1;i<=letters_blocknum.size();++i)
		{
	        if(key2[j]==(char)i+48)
	  		{
				newcode += letters_blocknum[i-1];
			}
		}
	}
	//cout<<" from key2: "<<newcode<<endl;
	return newcode;
}

void decrypt_transcipher(string& mytext, int blocknum, string& key2)
{
    string newtext = "";
	string letters_blocknum = "";
	int place = 0;
    for (int i=0;i<mytext.size();++i)
    {
		letters_blocknum += mytext[i];
		if ((i+1)%blocknum == 0)
		{
			newtext += tranxposition(key2,letters_blocknum);
			letters_blocknum = "";
			place = i;
		}
    }

	//what is left from mytext remove the biggest ones
    string keylast="";
    for (int i=0;i<=mytext.size()-place;++i)
    {
		if ((int)key2[i]-48<mytext.size()-place)
		{
			keylast += key2[i]; //create new key for last letters
		}
    }

	for (int i=place;i<mytext.size();++i)
    {
        letters_blocknum += mytext[i]; //create new stringwith the size of last letters
	}
	newtext += tranxposition(keylast,letters_blocknum); //transposition letters depending ot he key

	cout<<endl<<" GIVEN TEXT : "<<mytext<<endl;
	cout<<" BLOCK NUMBER : "<<blocknum<<endl;
	cout<<" KEY : "<<key2<<endl;
	cout<<" FINAL TEXT : "<<newtext<<endl<<endl;
}

string tranxposition(string& key2, string& letters_blocknum)
{
    string newcode="";
    for (int i=1;i<=letters_blocknum.size();++i)
	{
		for (int j=0;j<key2.size();++j)
		{
	        if(key2[j]==(char)i+48)
	  		{
				newcode += letters_blocknum[j];
			}
		}
	}
	//cout<<" from key2: "<<newcode<<endl;
	return newcode;
}
