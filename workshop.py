# -*- coding: utf-8 -*-
import os
import re
import collections
# ---------------------------------------------------------------CLANG XML CREATION----------------------------------------------------------
os.system("clang -Xclang -ast-dump C:/test1.cpp > C:/test1.xml")
xmllist = []
with open("C:/test1.xml") as myfile:
    for line in myfile:
        xmllist.append(line)

# -----------------------------------------------------DICTIONARY CREATION AND PASSING ARGS---------------------------------------------------------------
# Functions που βαζουν τις πληροφοριες του xml clang και τις προσθετουν σε ξεχωριστα dictionaries με τα απαραιτητα στοιχεια στο καθενα.

ast_classes = collections.OrderedDict()


# Function για να παιρνει τις κλασεις και να τις βαζει σε ενα dictionary με hex ως key και αρχικη γραμμη, τελικη γραμμη και ονομα ως value.
def class_match(line):
    if re.search(r"(CXXRecordDecl)(\W.*?parent\W)(.*?)(\W.*?prev\W.*?\W)(.*?line:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?\W.*?ass\W)(.*?)(\W)",line):
        inherited_classes = re.search(r"(CXXRecordDecl)(\W.*?parent\W)(.*?)(\W.*?prev\W.*?\W)(.*?line:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?\W.*?ass\W)(.*?)(\W)",line)
        ast_classes[inherited_classes.group(10)] = [inherited_classes.group(6), inherited_classes.group(8),inherited_classes.group(3)]

    elif re.search(r"(CXXRecordDecl)(\W)(.*?)(\Wprev.*?line:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?:.*?ass\W)(.*?)(\W)",line):
        prev_classes = re.search(r"(CXXRecordDecl)(\W)(.*?)(\Wprev.*?line:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?:.*?ass\W)(.*?)(\W)", line)
        ast_classes[prev_classes.group(9)] = [prev_classes.group(5), prev_classes.group(7), prev_classes.group(3)]

    elif re.search(r"(CXXRecordDecl)(\W)(.*?)(\W.*?line:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?:.*?ass\W)(.*?)(\W)", line):
        just_classes = re.search(r"(CXXRecordDecl)(\W)(.*?)(\W.*?line:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?:.*?ass\W)(.*?)(\W)", line)
        ast_classes[just_classes.group(9)] = [just_classes.group(5), just_classes.group(7), just_classes.group(3)]


ast_struct = collections.OrderedDict()


# Function για να παιρνει τα structures και να τα βαζει σε ενα dictionary με hex ως key και αρχικη γραμμη, τελικη γραμμη και ονομα ως value
def struct_match(line):
    if re.search(r"(CXXRecordDecl)(\W)(.*?)(\W.*?line:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?:.*?\W)(struct)(\W)(.*?)(\W)", line):
        struct_results = re.search(r"(CXXRecordDecl)(\W)(.*?)(\W.*?line:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?:.*?\W)(struct)(\W)(.*?)(\W)", line)
        ast_struct[struct_results.group(9)] = [struct_results.group(5), struct_results.group(7), struct_results.group(3)]


ast_method = collections.OrderedDict()


# Function για να παιρνει τα methods και να τα βαζει σε ενα dictionary με hex ως key και αρχικη γραμμη, τελικη γραμμη και ονομα ως value
def method_match(searchname, line):
    if re.search(r"(" + searchname + ")(\W.*?parent\W)(.*?)(\Wprev.*?line:)(.*?)(:.*?line:)(.*?)(:.*?used\W)(.*?)(\W')(.*?)(')", line):
        method_results = re.search(r"(" + searchname + ")(\W.*?parent\W)(.*?)(\Wprev.*?line:)(.*?)(:.*?line:)(.*?)(:.*?used\W)(.*?)(\W')(.*?)(')", line)
        ast_method[method_results.group(9)] = [method_results.group(5), method_results.group(7), method_results.group(3)]


ast_function = collections.OrderedDict()


# Function για να παιρνει τα functions και να τα βαζει σε ενα dictionary με ονομα function ως key και αρχικη γραμμη, τελικη γραμμη value
def function_match(searchname, line):
    if re.search(r"(" + searchname + ")(.*?line:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?used\W)(.*?)(\W')(.*?)(')", line):
        function_results = re.search(r"(" + searchname + ")(.*?line:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?used\W)(.*?)(\W')(.*?)(')", line)
        ast_function[function_results.group(7)] = [function_results.group(3), function_results.group(5)]


ast_constructor = collections.OrderedDict()


# Function για να παιρνει τα constructors και να τα βαζει σε ενα dictionary με hex ως key και αρχικη γραμμη, τελικη γραμμη και ονομα ως value
def constructor_match(line):
    if re.search(r"(CXXConstructorDecl)(\W)(.*?)(\W.*?:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?:.*?\W)(.*?\W.*?\W)(\W')(.*?)(')", line):
        const_results = re.search(r"(CXXConstructorDecl)(\W)(.*?)(\W.*?:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?:.*?\W)(.*?\W.*?\W)(\W')(.*?)(')", line)
        ast_constructor[const_results.group(9)] = [const_results.group(5), const_results.group(7), const_results.group(3)]


# -----------------------------------------------------------------------------------------------------------------------------------------

for line in xmllist:
    class_match(line)
    struct_match(line)
    function_match("FunctionDecl", line)
    method_match("CXXMethodDecl", line)
    constructor_match(line)

# -------------------------------------------------------------------------CPPCHECK PART----------------------------------------------------------------

os.system("\"C:/Program Files (x86)/Cppcheck/cppcheck.exe\" --xml-version=2 --enable=all C:/test1.cpp 2> C:/test1_cppcheck_results.xml")
cppcheckxml_list = []
with open("C:/test1_cppcheck_results.xml") as my_file:
    for line in my_file:
        cppcheckxml_list.append(line)

#Function που τραβαει απο το cppcheck το id του error, την κατηγορια και την γραμμη του λαθους και την αποθηκευει σε μια νεα λιστα.
def cppcheck_result_parser(cppcheckxml_list):
    _list = []
    all_errors_dict = {}
    for line in cppcheckxml_list:
        if re.search(r"(error id=\")(.*?)(\".*?severity=\")(.*?)(\".*?\")", line):
            error_results = re.search(r"(error id=\")(.*?)(\".*?severity=\")(.*?)(\".*?\")", line)
            _list[:] = []
            _list.append(error_results.group(2))
            _list.append(error_results.group(4))
        if re.search(r"(location file=.*?=\")(.*?)(\"/)", line):
            error_line = re.search(r"(location file=.*?=\")(.*?)(\"/)", line)
            all_errors_dict[error_line.group(2)] = _list
    return all_errors_dict

all_errors_dict = cppcheck_result_parser(cppcheckxml_list)

# ------------------------------------Βασικές μετρήσεις του κώδικα απο το clang xml----------------------------------------
def print_metrics():
    print ("Number of Functions: " + len(ast_function).__str__())
    print ("Number of Classes: " + len(ast_classes).__str__())
    print ("Number of Methods: " + len(ast_method).__str__())
    print ("Number of Structs: " + len(ast_struct).__str__())
    print ("Number of Constructor: " + len(ast_constructor).__str__())

def space_num(path):
    space = 0
    with open(path) as myfile:
        for line in myfile:
            finded = line.find(" ")
            if finded != -1 and finded != 0:
                space += 1
    return space

s = space_num("C:/test1.cpp")

# -------------------------Μετρήσεις αριθμού γραμμών και εισαγωγή τους στο dictionary-------------------------------------------------------
# Aριθμός γραμμών ανα κλάση/μέθοδο/function/constructor/struct
def func_lines(ast_function):
    for key, val in ast_function.items():
        difference = 0
        for num, line in enumerate(val, 0):
            start_line = val[0]
            end_line = val[1]
            difference = int(end_line) - int(start_line)
        ast_function[key].append("sum of lines: " + difference.__str__())


func_lines(ast_function)


def class_lines(ast_classes):
    for key, val in ast_classes.items():
        difference = 0
        for num, line in enumerate(val, 0):
            start_line = val[0]
            end_line = val[1]
            difference = int(end_line) - int(start_line)
        ast_classes[key].append("sum of lines: " + difference.__str__())


class_lines(ast_classes)


def meth_lines(ast_method):
    for key, val in ast_method.items():
        difference = 0
        for num, line in enumerate(val, 0):
            start_line = val[0]
            end_line = val[1]
            difference = int(end_line) - int(start_line)
        ast_method[key].append("sum of lines: " + difference.__str__())


meth_lines(ast_method)


def str_lines(ast_struct):
    for key, val in ast_struct.items():
        difference = 0
        for num, line in enumerate(val, 0):
            start_line = val[0]
            end_line = val[1]
            difference = int(end_line) - int(start_line)
        ast_struct[key].append("sum of lines: " + difference.__str__())


str_lines(ast_struct)

def constr_lines(ast_constructor):
    for key, val in ast_constructor.items():
        difference = 0
        for num, line in enumerate(val, 0):
            start_line = val[0]
            end_line = val[1]
            difference = int(end_line) - int(start_line)
        ast_constructor[key].append("sum of lines: " + difference.__str__())


constr_lines(ast_constructor)


# Συνολικός αριθμός γραμμών
def total_number_of_lines(path):
    num_lines = sum(1 for line in open(path))
    return num_lines

nol = total_number_of_lines("C:/test1.cpp")
# ----------------------------------------------------------------------------------------------------------------------------------------------------
# Συγκριση του καθε dictionary με τα αποτελεσματα που εφερε απο το clang - xml και του dictionary με τα errors και αντιστοιχιση του ονοματος του

errorlog = {}

def func(errorlog):
 for key, val in ast_function.items():
    for j, line in enumerate(val, 0):
        function_with_error = key
        startingpoint = val[0]
        endingpoint = val[1]
        for keyx, val1 in all_errors_dict.items():
            for i, line1 in enumerate(val1, 0):
                errorname = val1[0]
                errorline = keyx
                if (int(errorline) <= int(endingpoint)) and (int(errorline) >= int(startingpoint)):
                    errorlog[function_with_error] = [("Line of error: ") + errorline, ("Name of error: ") + errorname]


def struct(errorlog):
 for key, val in ast_struct.items():
    for j, line in enumerate(val, 0):
        struct_with_error = key
        startingpoint = val[0]
        endingpoint = val[1]
        for keyx, val1 in all_errors_dict.items():
            for i, line1 in enumerate(val1, 0):
                errorname = val1[0]
                errorline = keyx
                if (int(errorline) <= int(endingpoint)) and (int(errorline) >= int(startingpoint)):
                    errorlog[struct_with_error] = [("Line of error: ") + errorline, ("Name of error: ") + errorname]


def const(errorlog):
 for key, val in ast_constructor.items():
    for j, line in enumerate(val, 0):
        const_with_error = key
        startingpoint = val[0]
        endingpoint = val[1]
        for keyx, val1 in all_errors_dict.items():
            for i, line1 in enumerate(val1, 0):
                errorname = val1[0]
                errorline = keyx
                if (int(errorline) <= int(endingpoint)) and (int(errorline) >= int(startingpoint)):
                    errorlog[const_with_error] = [("Line of error: ") + errorline, ("Name of error: ") + errorname]


def meth(errorlog):
 for key, val in ast_method.items():
    for j, line in enumerate(val, 0):
        meth_with_error = key
        startingpoint = val[0]
        endingpoint = val[1]
        for keyx, val1 in all_errors_dict.items():
            for i, line1 in enumerate(val1, 0):
                errorname = val1[0]
                errorline = keyx
                if (int(errorline) <= int(endingpoint)) and (int(errorline) >= int(startingpoint)):
                    errorlog[meth_with_error] = [("Line of error: ") + errorline, ("Name of error: ") + errorname]


def classs(errorlog):
 for key, val in ast_classes.items():
    for j, line in enumerate(val, 0):
        class_with_error = key
        startingpoint = val[0]
        endingpoint = val[1]
        for keyx, val1 in all_errors_dict.items():
            for i, line1 in enumerate(val1, 0):
                errorname = val1[0]
                errorline = keyx
                if (int(errorline) <= int(endingpoint)) and (int(errorline) >= int(startingpoint)):
                    errorlog[class_with_error] = [("Line of error: ") + errorline, ("Name of error: ") + errorname]

#--------------------------------Results Printing-----------------------------------------------------------------------
print "--------------Metrics---------------"
print_metrics()
print "Number of Spaces: " + s.__str__()
print "Number of Lines: " + nol.__str__()
print "--------------Errors----------------"
func(errorlog)
struct(errorlog)
const(errorlog)
meth(errorlog)
classs(errorlog)


for keys, values in errorlog.items():
    print (keys, values)
