import re
import collections

blamelist = []

with open("C:/Blames.xml") as myfile:
    for line in myfile:
        blamelist.append(line)

blame_info_Dict = collections.OrderedDict()

def blame_match(line, blame_info_Dict):
    if re.search(r"(\()(.*?)(\W+)([0-9].*?-.*?-.*?)(\W.*?:.*?:.*?\W+.*?\W+)(.*?)(\))", line):
        blamegroups = re.search(r"(\()(.*?)(\W+)([0-9].*?-.*?-.*?)(\W.*?:.*?:.*?\W+.*?\W+)(.*?)(\))", line)
        blame_info_Dict[blamegroups.group(6)] = [blamegroups.group(2), blamegroups.group(4)]

for line in blamelist:
    blame_match(line, blame_info_Dict)

'''
for keys, value in blame_info_Dict.items():
    print (keys, value)
'''
bot_list = []
def num_of_commits(blame_info_Dict):
    commits = 0
    for key, val in blame_info_Dict.items():
        for num, line in enumerate(val, 1):
            if val[0] not in bot_list:
                bot_list.append(val[0])
                commits +=1
    return commits


people_that_committed = bot_list
commits = num_of_commits(blame_info_Dict)
print "People that Committed: ", people_that_committed.__str__()
print "Number of Commits: ",  commits.__str__()
