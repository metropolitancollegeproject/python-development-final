# -*- coding: utf-8 -*-
import unittest
import workshop
import re
import GitCommitTracker

class workshoptesting(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

#--------------------------- Κωνσταντίνος Παναγιώτου---------------------------------------------------------------------------------
    def test_constr_lines(self):
        ast_constructor =             {'fpos<_StateT>': ['133', '134', '0x1df74936c80', 'sum of lines: 1'],
                                      'pair<_T1, _T2>': ['118', '119', '0x1df74a32100', 'sum of lines: 1'],
                                      'reverse_iterator<_Iterator>': ['140', '141', '0x1df74a4ccb0', 'sum of lines: 1'],
                                      'insert_iterator<_Container>': ['615', '616', '0x1df74a44740', 'sum of lines: 1'],
                                      '__normal_iterator<_Iterator, _Container>': ['745', '749', '0x1df74a59430', 'sum of lines: 4'],
                                      '_Iter_comp_iter<_Compare>': ['115', '117', '0x1df74a7eed0', 'sum of lines: 2'],
                                      '_Iter_comp_val<_Compare>': ['137', '139', '0x1df74a84100', 'sum of lines: 2'],
                                      '_Val_comp_iter<_Compare>': ['162', '164', '0x1df74a7b980', 'sum of lines: 2'],
                                      '_Iter_equals_val<_Value>': ['187', '189', '0x1df74a7d250', 'sum of lines: 2'],
                                      '_Iter_equals_iter<_Iterator1>': ['207', '209', '0x1df74a7a4c0', 'sum of lines: 2'],
                                      '_Iter_pred<_Predicate>': ['227', '229', '0x1df74a7b5d0', 'sum of lines: 2'],
                                      '_Iter_comp_to_val<_Compare, _Value>': ['248', '250', '0x1df74a767c0', 'sum of lines: 2'],
                                      '_Iter_comp_to_iter<_Compare, _Iterator1>': ['269', '271', '0x1df74a91fe0', 'sum of lines: 2'],
                                      '_Iter_negate<_Predicate>': ['289', '291', '0x1df74a93470', 'sum of lines: 2'],
                                      'allocator<type-parameter-0-0>': ['115', '116', '0x1df74b5dcd0', 'sum of lines: 1'],
                                      'pointer_to_unary_function<_Arg, _Result>': ['827', '829', '0x1df74cb4080', 'sum of lines: 2'],
                                      'pointer_to_binary_function<_Arg1, _Arg2, _Result>': ['853', '855', '0x1df74cb5a30', 'sum of lines: 2'],
                                      'mem_fun_t<_Ret, _Tp>': ['942', '944', '0x1df74cb9950', 'sum of lines: 2']}
        self.assertEqual(workshop.constr_lines(ast_constructor),None)



    def test_func_lines(self):
        ast_function =    {'vswprintf': ['14', '54', 'sum of lines: 40'],
                          '__is_null_pointer': ['149', '151', 'sum of lines: 2'],
                          '__iterator_category': ['203', '205', 'sum of lines: 2'],
                          '__distance': ['89', '97', 'sum of lines: 8'],
                          'distance': ['113', '119', 'sum of lines: 6'],
                          'operator-': ['922', '926', 'sum of lines: 4'],
                          'max': ['218', '227', 'sum of lines: 9'],
                          '__niter_base': ['281', '283', 'sum of lines: 2'],
                          '__miter_base': ['292', '294', 'sum of lines: 2'],
                          '__copy_move_a': ['388', '401', 'sum of lines: 13'],
                          '__copy_move_a2': ['433', '439', 'sum of lines: 6'],
                          'copy': ['459', '471', 'sum of lines: 12'],
                          '__copy_move_backward_a': ['588', '603', 'sum of lines: 15'],
                          '__copy_move_backward_a2': ['606', '612', 'sum of lines: 6']}
        self.assertEqual(workshop.func_lines(ast_function), None)



    def test_class_lines(self):
        ast_classes =  {'fpos': ['112', '207', '0x2bc7179a1a0', 'sum of lines: 95'],
                        'exception': ['60', '69', '0x2bc72f27090', 'sum of lines: 9'],
                        'bad_exception': ['73', '84', '0x2bc72f20880', 'sum of lines: 11'],
                        'reverse_iterator': ['97', '278', '0x2bc72fce3a0', 'sum of lines: 181'],
                        'back_insert_iterator': ['415', '477', '0x2bc72fd18d0', 'sum of lines: 62'],
                        'front_insert_iterator': ['506', '567', '0x2bc72fe37a0', 'sum of lines: 61'],
                        'insert_iterator': ['600', '681', '0x2bc72fe5010', 'sum of lines: 81'],
                        '__normal_iterator': ['721', '807', '0x2bc72fe7158', 'sum of lines: 86'],
                        'bad_alloc': ['54', '65', '0x2bc730f3ce0', 'sum of lines: 11'],
                        'new_allocator': ['58', '135', '0x2bc730f8bc0', 'sum of lines: 77'],
                        'allocator': ['92', '124', '0x2bc730f7430', 'sum of lines: 32']}
        self.assertEqual(workshop.class_lines(ast_classes), None)


    def test_meth_lines(self):
        ast_method = {'is': ['41', '44', '0x2987bdf2e48', 'sum of lines: 3']}
        self.assertEqual(workshop.meth_lines(ast_method), None)


    def test_str_lines(self):
        ast_struct = {'struct': ['46', '50', '0x1dc625401b8', 'sum of lines: 4']}
        self.assertEqual(workshop.str_lines(ast_struct), None)



    def test_space_num(self):
        path = "C:/test1.cpp"
        self.assertEqual(workshop.space_num(path), 324)
#-----------------------------------------Σπυρίδον Μακρής & Ορφέας Ορφανόγιαννης------------------------------------------------
    def test_classs(self):
            errorlog = {'vector': ['Line of error: 231', 'Name of error: missingInclude'],
                             'binder1st': ['Line of error: 110', 'Name of error: missingInclude'],
                             'basic_streambuf': ['Line of error: 231', 'Name of error: missingInclude'],
                             'basic_string': ['Line of error: 231', 'Name of error: missingInclude'],
                             'ios_base': ['Line of error: 231', 'Name of error: missingInclude'],
                             'locale': ['Line of error: 231', 'Name of error: missingInclude'],
                             '__ctype_abstract_base': ['Line of error: 231', 'Name of error: missingInclude'],
                             'basic_ostream': ['Line of error: 231', 'Name of error: missingInclude'],
                             'fpos': ['Line of error: 198', 'Name of error: missingInclude'],
                             'overflow_error': ['Line of error: 231', 'Name of error: missingInclude'],
                             'facet': ['Line of error: 388', 'Name of error: missingInclude'],
                             'basic_ios': ['Line of error: 231', 'Name of error: missingInclude'],
                             'istreambuf_iterator': ['Line of error: 110', 'Name of error: missingInclude'],
                             'basic_istream': ['Line of error: 231', 'Name of error: missingInclude'],
                             'ostreambuf_iterator': ['Line of error: 231', 'Name of error: missingInclude'],
                             'new_allocator': ['Line of error: 110', 'Name of error: missingInclude'],
                             'runtime_error': ['Line of error: 198', 'Name of error: missingInclude'],
                             'reverse_iterator': ['Line of error: 231', 'Name of error: missingInclude'],
                             'allocator': ['Line of error: 110', 'Name of error: missingInclude']}
            self.assertEqual(workshop.classs(errorlog), None)

    def test_meth(self):
            errorlog = {'':[''],}
            self.assertEqual(workshop.meth(errorlog), None)
    def test_const(self):
            errorlog = {'':[''],}
            self.assertEqual(workshop.meth(errorlog), None)

    def test_func(self):
        errorlog = {'__ostream_insert' :['Line of error: 110', 'Name of error: missingInclude'],
                    '__copy_move_a' :['Line of error: 388', 'Name of error: missingInclude'],
                    '__miter_base' :['Line of error: 293', 'Name of error: missingInclude'],
                    'uninitialized_copy' :['Line of error: 110', 'Name of error: missingInclude']}
        self.assertEqual(workshop.func(errorlog), None)

    def test_struct(self):
        errorlog = {'': [''], }
        self.assertEqual(workshop.struct(errorlog), None)

#-------------------------------------------Κωνσταντίνος Λάμπης-----------------------------------------------------------------
    def test_class_match(self):
        line = "CXXRecordDecl 0x1c2d55f2960 parent 0x1c2d55eb790 prev 0x1c2d55ebaa0 <line:474:3, line:509:3> line:474:17 referenced class idf"
        ast_classes = {'idf': ['474', '509', '0x1c2d55eb790']}
        inherited_classes = re.search(r"(CXXRecordDecl)(\W.*?parent\W)(.*?)(\W.*?prev\W.*?\W)(.*?line:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?\W.*?ass\W)(.*?)(\W)", line)
        self.assertEqual((ast_classes.items()), [('idf', ['474', '509', '0x1c2d55eb790'])])
        self.assertEqual((workshop.class_match(line)), None)

    def test_class_match2(self):
        line = "CXXRecordDecl 0x1c2d5626cb0 prev 0x1c2d521df00 <line:222:3, line:867:3> line:222:9 referenced class ios_base"
        ast_classes = {'ios_base': ['222', '867', '0x1c2d5626cb0']}
        prev_classes = re.search(r"(CXXRecordDecl)(\W)(.*?)(\Wprev.*?line:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?:.*?ass\W)(.*?)(\W)", line)
        self.assertEqual((ast_classes.items()), [('ios_base', ['222', '867', '0x1c2d5626cb0'])])
        self.assertEqual((workshop.class_match(line)), None)

    def test_class_match3(self):
        line = "CXXRecordDecl 0x25f3af0 <line:428:9, line:431:1> line:428:16 class localeinfo"
        ast_classes = {'localeinfo': ['428', '431', '0x25f3af0']}
        just_classes = re.search(r"(CXXRecordDecl)(\W)(.*?)(\W.*?line:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?:.*?ass\W)(.*?)(\W)", line)
        self.assertEqual((ast_classes.items()), [('localeinfo', ['428', '431', '0x25f3af0'])])
        self.assertEqual((workshop.class_match(line)), None)

    def test_struct_match(self):
        line = "| | |-CXXRecordDecl 0x1c2d5218c20 <line:112:5, line:207:5> line:112:11 struct fpos"
        ast_struct = {'fpos': ['112', '207', '0x1c2d5218c20']}
        struct_results = re.search(r"(CXXRecordDecl)(\W)(.*?)(\W.*?line:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?:.*?\W)(struct)(\W)(.*?)(\W)", line)
        self.assertEqual((ast_struct.items()), [('fpos', ['112', '207', '0x1c2d5218c20'])])
        self.assertEqual((workshop.struct_match(line)), None)

    def test_method_match(self):
        searchname = "CXXMethodDecl"
        line = "|-CXXMethodDecl 0x1c2d5aa3cb8 parent 0x1c2d5aa1ab8 prev 0x1c2d5aa27a0 <line:59:1, line:62:1> line:59:19 used setcode 'void (string &)'"
        ast_method = {'setcode': ['59', '62', '0x1c2d5aa1ab8']}
        method_results = re.search(r"(CXXMethodDecl)(\W.*?parent\W)(.*?)(\Wprev.*?line:)(.*?)(:.*?line:)(.*?)(:.*?used\W)(.*?)(\W')(.*?)(')", line)
        self.assertEqual((ast_method.items()), [('setcode', ['59', '62', '0x1c2d5aa1ab8'])])
        self.assertEqual((workshop.method_match(searchname, line)), None)

    def test_function_match(self):
        searchname = "FunctionDecl"
        line = "FunctionDecl 0x23af6750ce0 <line:909:3, line:918:7> line:913:7 used getchar1 'void (char)'"
        ast_function = {'getchar1': ['909', '918']}
        function_results = re.search(r"(FunctionDecl)(.*?line:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?used\W)(.*?)(\W')(.*?)(')", line)
        self.assertEqual((ast_function.items()), [('getchar1', ['909', '918'])])
        self.assertEqual((workshop.function_match(searchname, line)), None)

    def test_constructor_match(self):
        line = "CXXConstructorDecl 0x2de9ab8 parent 0x2dd56f0 prev 0x2de8550 <line:187:1, line:192:1> line:187:10 used Library 'void (int, string &, vector<class Book> &)'"
        ast_constructor = {'Library': ['187', '192', '0x2de9ab8']}
        const_results = re.search(r"(CXXConstructorDecl)(\W)(.*?)(\W.*?:)(.*?)(:.*?line:)(.*?)(:.*?line:.*?:.*?\W)(.*?\W.*?\W)(\W')(.*?)(')", line)
        self.assertEqual((ast_constructor.items()), [('Library', ['187', '192', '0x2de9ab8'])])
        self.assertEqual((workshop.constructor_match(line)), None)

    def test_cppcheck_results_parser(self):
        cppcheckxml_list = []
        all_errors_dict = {'705': ['missingInclude', 'information']}
        self.assertEqual((all_errors_dict.items()), [('705', ['missingInclude', 'information'])])
        self.assertEqual(workshop.cppcheck_result_parser(cppcheckxml_list), None)

    def test_blame_match(self):
        line = "0bc84d04395569f107291c3c9d8f6764ad7b5417 (KostasNouk 2017-01-14 17:09:52 +0200  14)     #Regular Expression για μεθοδους που ανηκουν σε κλασεις"
        blame_info_Dict = {'343': ['KostasNouk', '2017-01-14']}
        blamegroups = re.search(r"(\()(.*?)(\W+)([0-9].*?-.*?-.*?)(\W.*?:.*?:.*?\W+.*?\W+)(.*?)(\))", line)
        self.assertEqual((blame_info_Dict.items()), [('343', ['KostasNouk', '2017-01-14'])])
        self.assertEqual(GitCommitTracker.blame_match(line, blame_info_Dict), None)

    def test_num_of_commits(self):
        bot_list = ['KostasNouk', 'SpyrosMakrhs', 'OrfeasOrf', 'KostasPanayotou']
        blame_info_Dict = {}
        commits = 4
        self.assertEqual(commits, 4)
        self.assertEqual(bot_list, ['KostasNouk', 'SpyrosMakrhs', 'OrfeasOrf', 'KostasPanayotou'])
        self.assertEqual((GitCommitTracker.num_of_commits(blame_info_Dict)), 0)

    def test_total_number_of_lines(self):
        path = "C:/test1.cpp"
        self.assertEqual(workshop.total_number_of_lines(path), 941)

















